﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SocialPlatforms;
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.Multiplayer;

public class ControllerScript : MonoBehaviour, RealTimeMultiplayerListener {

	public Transform prefab = null;
	public Transform player = null;
	private Elsicity horizontal = null;
	private Elsicity vertical = null;
	float timer = 0f;
	
	// Use this for initialization
	void Start () {
		
	}
	
	// player movement with slider controll
	void Update () {
		if(horizontal == null || vertical == null){
			//player = Instantiate(prefab, new Vector3(0f, 2f, 0f), Quaternion.identity);
			Authenticate();

			horizontal = transform.Find("Horizontal").GetComponent<Elsicity>();
			vertical = transform.Find("Vertical").GetComponent<Elsicity>();
		}else{
			if(player != null){
				player.Translate(Vector3.forward * vertical.value * Time.deltaTime);
				player.Translate(Vector3.right * horizontal.value * Time.deltaTime * 2.5f);

				if(timer > 0f){
					timer -= Time.deltaTime;
				}else{
					timer = 0.035f;
					bool reliability = true;
					string data = "Position : " + player.position.x + ":" + player.position.y +":" + player.position.z;
					byte[] bytedata = System.Text.ASCIIEncoding.Default.GetBytes(data); 
					//byte[] bytesy = new byte[1];
					PlayGamesPlatform.Instance.RealTime.SendMessageToAll(reliability, bytedata);
				}
			}
		}
	}

	//au
	public void Authenticate(){
		 PlayGamesClientConfiguration config = new PlayGamesClientConfiguration.Builder()
        .Build();

    PlayGamesPlatform.InitializeInstance(config);
    // recommended for debugging:
    PlayGamesPlatform.DebugLogEnabled = true;
    // Activate the Google Play Games platform
    PlayGamesPlatform.Activate();

	PlayGamesPlatform.Instance.Authenticate((bool success) => {
			if(success){
				Debug.Log("Autentikasi sukses");
				CreateQuickGame();
			}else{
				Debug.Log("Autentikasi Gagal");
			}
		});
	}

	void CreateQuickGame(){
		const int MinOpponents = 1, MaxOpponents = 7;
		const int MinOpponentsInvite = 1, MaxOpponentsInvite = 3;
    	const int GameVariant = 0;
		PlayGamesPlatform.Instance.RealTime.CreateQuickGame(MinOpponents, MaxOpponents, GameVariant, this);
		PlayGamesPlatform.Instance.RealTime.CreateWithInvitationScreen(MinOpponentsInvite, MaxOpponentsInvite, GameVariant, this);
	}

	#region RealTimeMultiplayerListener implementation
	private bool isRoomSetup = false;
    void RealTimeMultiplayerListener.OnRoomSetupProgress(float percent)
    {
        //throw new System.NotImplementedException();
		if(percent >= 20){
			isRoomSetup = true;
			Debug.Log("Finding Match");
			PlayGamesPlatform.Instance.RealTime.ShowWaitingRoomUI();
		}
    }

	private bool isConnected = false;
    void RealTimeMultiplayerListener.OnRoomConnected(bool success)
    {
        //throw new System.NotImplementedException();
		if(success){
			//instance your player on network
			isConnected = true;

			player = Instantiate(prefab, new Vector3(0f, 4f, 0f), Quaternion.identity);
			player.name = PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId;

			bool reliability = true;
			string data = "Instantiate:0:1:2";
			byte[] bytedata = System.Text.ASCIIEncoding.Default.GetBytes(data); 
			PlayGamesPlatform.Instance.RealTime.SendMessageToAll(reliability, bytedata);
		}else{
			isConnected = false;
			CreateQuickGame();
		}
    }

    void RealTimeMultiplayerListener.OnLeftRoom()
    {
    	//throw new System.NotImplementedException();
		isConnected = false;
    }

    void RealTimeMultiplayerListener.OnParticipantLeft(Participant participant)
    {
        //throw new System.NotImplementedException();
    }

    void RealTimeMultiplayerListener.OnPeersConnected(string[] participantIds)
    {
        //throw new System.NotImplementedException();
    }

    void RealTimeMultiplayerListener.OnPeersDisconnected(string[] participantIds)
    {
        //throw new System.NotImplementedException();
    }

    void RealTimeMultiplayerListener.OnRealTimeMessageReceived(bool isReliable, string senderId, byte[] data)
    {
        //throw new System.NotImplementedException();
		if(!PlayGamesPlatform.Instance.RealTime.GetSelf().ParticipantId.Equals(senderId)){
			string rawdata = System.Text.ASCIIEncoding.Default.GetString(data);
			string[] sliced = rawdata.Split(new string[1] {":"}, System.StringSplitOptions.RemoveEmptyEntries);

			if(sliced[0].Contains("Instantiate")){
				Transform naming = Instantiate(prefab, new Vector3(0f, 4f, 0f), Quaternion.identity);
				naming.name = senderId;

				naming.GetChild(0).gameObject.SetActive(false);
			}else if(sliced[0].Contains("Position")){							//change data var to sliced
				Transform target = GameObject.Find(senderId).transform;
				if(target == null){
					return;
				}
				Vector3 newpos = new Vector3
				(
					System.Convert.ToSingle(data[1]), 						//change data var to sliced
					System.Convert.ToSingle(data[2]),						//change data var to sliced
					System.Convert.ToSingle(data[3])						//change data var to sliced
				);
					target.position = Vector3.Lerp(target.position, newpos, Time.deltaTime * 3.0f);
				}
			}
		#endregion
	}
}
