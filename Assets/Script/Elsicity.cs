﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class Elsicity : MonoBehaviour, IPointerDownHandler, IPointerUpHandler{

	public float value = 0f;
	private Slider slider = null;
	private bool isPressed = false;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if(slider == null){
			slider = GetComponent<Slider>();
		}else{
			if(!isPressed){
				slider.value = Mathf.Lerp(slider.value, 0f, Time.deltaTime * 3.0f);
			}
		}
		value = slider.value;
	}

	#region IPointerDownHandler implementation
	public void OnPointerDown(PointerEventData eventData){
		//throw new System.NotImplementedException();
		isPressed = true;
		Debug.Log("Move Object 1");
	}
	#endregion

	#region  IPointerUpHandler implementation
	public void OnPointerUp(PointerEventData eventData){
		//throw new System.NotImplementedException();
		isPressed = false;
		Debug.Log("Move Object 2");
	}
	#endregion
}
